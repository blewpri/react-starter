# **React** Starter Project

Project starting point with Typscript, Redux, Storybook, Enzyme...

## Quickstart

Run `npm i`, then:
* `npm run start`: Run app locally ([http://localhost:3000/](http://localhost:3000/))
* `npm run test`: Run tests

## About Project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). 